#!/bin/bash

connected=1
until (( $connected == 0 )); do
	ping -qc1 google.com
	connected=$?
	sleep 1
done
